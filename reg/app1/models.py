from django.db import models

# Create ffyour models here.
from django.db import models
from django.contrib.auth.models import User

class LoginLogoutEvent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event_type = models.CharField(max_length=10)  # 'login' or 'logout'
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user.username} - {self.event_type} - {self.timestamp}'
