import datetime
import time
from django.utils import timezone
from django.contrib.auth.models import User
from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth import authenticate,login,logout
import requests
from .models import LoginLogoutEvent

# Create your views here.

def signPage(requests):
   if requests.method=='POST':
        uname=requests.POST.get('username')
        email=requests.POST.get('email')
        pass1=requests.POST.get('password1')
        pass2=requests.POST.get('password2')
        if pass1!=pass2:
            return HttpResponse("Your password and confrom password are not Same!!")
        else:
            my_user=User.objects.create_user(uname,email,pass1)
            my_user.save()
            return redirect('login')
   return render (requests,'ssgnm.html')

  
def custom_login(requests):
    if requests.method=='POST':
        username=requests.POST.get('username')
        pass1=requests.POST.get('pass')
        user=authenticate(requests,username=username,password=pass1)
        if user is not None:
            login(requests,user)
            x=time.time()
            timestamp = x
            datetime_obj = datetime.datetime.utcfromtimestamp(timestamp)
            return HttpResponse(datetime_obj)
            return redirect('home')
        else:
            return HttpResponse ("Username or Password is incorrect!!!")

    return render (requests,'login.html')
    
def homePage(requests):
    return render(requests,'home.html')
def custom_logout(requests):
      logout(requests)
      return redirect('login')
